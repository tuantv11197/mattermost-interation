// Trong tệp store/module.js

// Khai báo trạng thái
import Vue from "vue";

const state = {
  count: 12,
  socket: null
};

// Khai báo mutations để thay đổi trạng thái
const mutations = {
  increment(state, payload) {
    state.count += payload;
  },
  connectWebSocket(state, payload) {
    state.socket = payload;
  }
};

// Khai báo actions để gọi mutations
const actions = {
  increment(context, payload) {
    context.commit('increment', payload);
  },

  async connectWebSocket(context, payload) {
    const socket = new WebSocket('ws://localhost:8065/api/v4/websocket')
    socket.onopen = (event) => {
      console.log(event);
      const msg= {
        "seq": 1,
        "action": "authentication_challenge",
        "data": {
          "token": payload
        }
      }
      socket.send(JSON.stringify(msg));
    };
    context.commit('connectWebSocket', socket)
    Vue.prototype.$socket = socket;
  },
};

export default {
  state,
  mutations,
  actions,
};
