
import Vue from 'vue';

console.log('ssss', process.env.SOCKET_URL)
const socket = new WebSocket(process.env.SOCKET_URL)
socket.onopen = (event) => {
  console.log(event);
  const msg= {
    "seq": 1,
    "action": "authentication_challenge",
    "data": {
      "token": localStorage.getItem('mattermost_user_token')
    }
  }
  socket.send(JSON.stringify(msg));
};
Vue.prototype.$socket = socket;
