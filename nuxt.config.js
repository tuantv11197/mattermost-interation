const cors = require('cors');
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'chat',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/socket-io.js', ssr: false },
    { src: '@/plugins/moment.js', ssr: false }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
  ],
  store: {
    modules: [
      // Thêm tên module Vuex và đường dẫn tới tệp của module
      '~/store/auth',
    ],
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  // axios: {
  //   // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
  //   baseURL: 'http://localhost:8065/api/',
  //   proxyHeaders: false,
  //   credentials: false,
  // },

  axios: {
    proxy: true // Can be also an object with default options
  },

  proxy: {
    '/api/': process.env.MATTERMOST_URL,
    '/socket.io': {
      target: process.env.MATTERMOST_URL, // Replace with your WebSocket server URL
      ws: true,
    }
  },

  env: {
    SOCKET_URL: process.env.SOCKET_URL,
    MATTERMOST_URL: process.env.MATTERMOST_URL,
    PERSONAL_ACCESS_TOKEN: process.env.PERSONAL_ACCESS_TOKEN,
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
